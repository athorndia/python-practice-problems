# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

def add_csv_lines(csv_lines):
    new_list = []
    for item in csv_lines:
        item = item.split(",")
        total = 0
        for number in item:
            total = total + int(number)
        new_list.append(total)
    return new_list

# Test cases
test_cases = [
    [],
    ["3", "1,9"],
    ["8,1,7", "10,10,10", "1,2,3"]
]

# Print out the input and output values for each test case
for i, test_case in enumerate(test_cases):
    result = add_csv_lines(test_case)
    print(f"Test case {i+1}: {test_case} => {result}")




'''
Here's a breakdown of how the function works:

1. Create an empty list new_list that will hold the sums of the CSV strings.
2. Iterate over each string in the input list csv_lines.
3. Split the string using the split() method to create a list of individual numbers.

    The split() method in Python is a built-in function for strings that allows you to split a string into a list of substrings based on a specified separator.
    By default, the separator is any whitespace character (such as space, tab, or newline), but you can also specify a custom separator.

4. Create a variable total and set it to 0.
5. Iterate over each number in the list of numbers.
6. Convert each number to an integer using the int() function and add it to total.
7. Append total to new_list.
8. Once all strings in csv_lines have been processed, return new_list.
'''
