# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    if len(password) < 6 or len(password) > 12:
        return False

    has_lower = False
    has_upper = False
    has_digit = False
    has_special = False

    for c in password:
        if c.islower():
            has_lower = True
        elif c.isupper():
            has_upper = True
        elif c.isdigit():
            has_digit = True
        elif c in ["$", "!", "@"]:
            has_special = True
    return has_lower and has_upper and has_digit and has_special


'''
This function takes a single parameter password and checks if it meets all the criteria for a valid password.
The function returns True if the password is valid, and False otherwise.

The function first checks if the length of the password is between 6 and 12 characters, inclusive.
If it is not, then the function returns False.

The function then uses four boolean variables (has_lower, has_upper, has_digit, and has_special)
to keep track of whether the password contains at least one lowercase letter, uppercase letter, digit, and special character, respectively.

The function then iterates over each character in the password and updates the boolean variables accordingly.
If all four boolean variables are True at the end of the loop, then the function returns True.
Otherwise, it returns False.
'''

password = "aBC123$"
print(check_password(password))
# return True

password = "abc123"
print(check_password(password))
# return False
