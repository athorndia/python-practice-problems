# Write a class that meets these requirements.
#
# Name:       ReceiptItem
#
# Required state:
#    * quantity, the amount of the item bought
#    * price, the amount each one of the things cost
#
# Behavior:
#    * get_total()          # Returns the quantity * price
        # quantity x price can be used to calculate the total cost of purchasing a certain number of items,
        # or the total revenue earned by selling a certain number of items at a given price.

# Example:
#    item = ReceiptItem(10, 3.45)
#
#    print(item.get_total())    # Prints 34.5


class ReceiptItem:
    def __init__(self, quantity, price):
        self.quantity = quantity
        self.price = price

    def get_total(self):
        return self.quantity * self.price

# Create new ReceiptItem using mock data
item = ReceiptItem(10, 3.45)

# Print total
print("$", item.get_total())    # Prints $34.5
