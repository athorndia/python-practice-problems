# Complete the sum_of_first_n_numbers function which accepts
# a numerical limit and returns the sum of the numbers from
# 0 up to and including limit.
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+1=1
#   * 2 returns 0+1+2=3
#   * 5 returns 0+1+2+3+4+5=15
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_first_n_numbers(limit):
    if limit < 0:
        return None
    else:
        total = 0
        for number in range(limit + 1):
            total = total + number
        return total

'''
1. Check if the limit is less than 0
2. If it is, return None
3. Otherwise, initialize a variable to keep track of the total sum
4. Loop through the numbers from 0 up to and including the limit
    If we want to include the value of limit in the sequence, we need to use limit + 1 as the ending value.
    This ensures that the sequence includes all the numbers from 0 up to and including limit.
5. For each number, add it to the total sum
6. After looping through all the numbers, return the total sum


'''
