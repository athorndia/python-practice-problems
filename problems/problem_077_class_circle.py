# Write a class that meets these requirements.
#
# Name:       Circle
#
# Required state:
#    * radius, a non-negative value
#
# Behavior:
#    * calculate_perimeter()  # Returns the length of the perimater of the circle
#    * calculate_area()       # Returns the area of the circle
#
# Example:
#    circle = Circle(10)
#
#    print(circle.calculate_perimeter())  # Prints 62.83185307179586
#    print(circle.calculate_area())       # Prints 314.1592653589793
#
# There is pseudocode for you to guide you.

import math

# class Circle
    # method initializer with radius
        # if radius is less than 0
            # raise ValueError
        # self.radius = radius

    # method calculate_perimeter(self)
        # returns 2 * math.pi * self.radius

    # method calculate_area(self)
        # returns math.pi * (self.radius squared)

class Circle:
    def __init__(self, radius):
        self.radius = radius

# The formula to calculate the perimeter (circumference) of a circle is: C = 2 * π * r
    def calculate_perimeter(self):
        return 2 * math.pi * self.radius

# The formula to calculate the area of a circle is: A = π * r^2
    def calculate_area(self):
        return math.pi * (self.radius ** 2)

# Create new Circle object using mock data
circle = Circle(10)

# Print out calculated perimeter and area based on mock data
print("Perimeter: ", circle.calculate_perimeter())      # Prints 62.83185307179586
print("Area: ", circle.calculate_area())                # Prints 314.1592653589793
