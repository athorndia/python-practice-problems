# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

def pad_left(number, length, pad):
    number_string = str(number)
    if len(number_string) >= length:
        return number_string
    else:
        number_pads = length - len(number_string)
        return pad * number_pads + number_string

'''
Here's how this function works:

Convert the input 'parameter number' number to a string using the str function.

Check if the length of the resulting string is greater than or equal to the desired 'parameter length' length.
If it is, return the string representation of the number as is.

If the length of the string is less than the desired length,
calculate the number of padding characters needed to pad the string by subtracting the length of the string from length.

Return a string consisting of num_pads padding characters followed by the string representation of the number.
For example, calling pad_left(10, 4, "*") would return the string "**10".

'''

def pad_left(number, length, pad):
    s = str(number)
    while len(s) < length:
        s = pad + s
    return s
