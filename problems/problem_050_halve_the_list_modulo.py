# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(list):
    half = len(list) // 2
    if len(list) % 2 == 0:
        return list[:half], list[half:]
    else:
        return list[:half+1], list[half+1:]

print(halve_the_list([1, 2, 3, 4]))
# result: [1, 2], [3, 4]
print(halve_the_list([1, 2, 3]))
# result: [1, 2], [3]


'''
The double slash // is called the integer division operator. It performs division and returns the integer quotient of the division, discarding any remainder.
In the given code, len(list) // 2 is used to calculate the index at which to split the list into two halves.
It divides the length of the list by 2 and returns the integer quotient as half.
This ensures that half is always an integer, even if the length of the list is odd.

If it's even, it returns two lists each with half of the original list.

If it's odd, it returns two lists with the extra item in the first list.


list[:half] returns a new list that contains the first half of the original list.
The : in the slicing notation indicates that we want to take a subset of the list from the beginning (index 0) up to, but not including, the half index.
'''
