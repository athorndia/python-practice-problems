# Write four classes that meet these requirements.
#
# Name:       Animal
#
# Required state:
#    * number_of_legs, the number of legs the animal has
#    * primary_color, the primary color of the animal
#
# Behavior:
#    * describe()       # Returns a string that describes that animal
#                         in the format
#                                self.__class__.__name__
#                                + " has "
#                                + str(self.number_of_legs)
#                                + " legs and is primarily "
#                                + self.primary_color
#
#
# Name:       Dog, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Bark!"
#
#
#
# Name:       Cat, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Miao!"
#
#
#
# Name:       Snake, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Sssssss!"


# Create parent class
class Animal:
    def __init__(self, number_of_legs, primary_color):
        self.number_of_legs = number_of_legs
        self.primary_color = primary_color

    '''
    Note that the describe method of each subclass will include
    the name of the subclass in the output,
    because we're using self.__class__.__name__} to get the name of the class.
    '''
    def describe(self):
        return (f"{self.__class__.__name__} has {str(self.number_of_legs)} "
                f"legs and is primarily {self.primary_color}")

# Create 3 subclasses


class Dog(Animal):
    def speak(self):
        return "Bark!"


class Cat(Animal):
    def speak(self):
        return "Miao!"


class Snake(Animal):
    def speak(self):
        return "Sssssss!"


# Create some new Animal objects and pass in mock data
animal = Animal(4, "brown")
dog = Dog(4, "white")
cat = Cat(4, "black")
snake = Snake(0, "green")

# Test the describe method for each object
print(animal.describe())    # Prints "Animal has 4 legs and is primarily brown"
print(dog.describe())       # Prints "Dog has 4 legs and is primarily white"
print(cat.describe())       # Prints "Cat has 4 legs and is primarily black"
print(snake.describe())     # Prints "Snake has 0 legs and is primarily green"
