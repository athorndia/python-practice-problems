# Complete the sum_of_first_n_even_numbers function which
# accepts a numerical count n and returns the sum of the
# first n even numbers
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+2=2
#   * 2 returns 0+2+4=6
#   * 5 returns 0+2+4+6+8+10=30
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_first_n_even_numbers(n):
    if n < 0:
        return None
    else:
        total = 0
        for number in range(n):
            total = total + (number + 1) * 2
        return total

'''
1. Check if n is less than 0
2. If it is, return None
3. Otherwise, initialize a variable to keep track of the total sum
4. Loop through the numbers from 1 up to n
5. For each number, multiply it by 2 and add it to the total sum
6. After looping through all the numbers, return the total sum


In the Python implementation, we first check if n is less than 0.
If it is, we return None. Otherwise, we initialize total to be 0.

Then, we loop through the numbers from 1 up to n.
We do this using the range function with a starting value of 1 and an ending value of n.
For each number, we multiply it by 2 and add it to total to keep track of the total sum.

Finally, after looping through all the numbers, we return total, which should be the sum of the first n even numbers.
'''
