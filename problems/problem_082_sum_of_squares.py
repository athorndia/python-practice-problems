def sum_of_squares(num1, num2):
    num1_squared = pow(num1, 2)
    num2_squared = pow(num2, 2)
    return num1_squared + num2_squared
