# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

def reverse_dictionary(dictionary):
    reverse_dict = {}
    for key in dictionary:
        value = dictionary[key]
        reverse_dict[value] = key
    return reverse_dict

'''
Here's how this function works:

1. Create an empty dictionary reverse_dict that will hold the reversed key-value pairs.
2. Iterate over the keys of the input dictionary.
3. For each key, get its corresponding value and add a new key-value pair to reverse_dict with the value as the key and the key as the value.
4. Once all key-value pairs in dictionary have been processed, return the reverse_dict.

'''

def reverse_dictionary(dictionary):
    new_dictionary = {}
    for key, value in dictionary.items():
        new_dictionary[value] = key
    return new_dictionary

'''
In Python, the items() method is a built-in function for dictionaries that returns a view object that contains the key-value pairs of the dictionary as tuples.
The view object can be used to iterate over the key-value pairs or convert them to a list or another dictionary.
'''

my_dict = {"apple": 1, "banana": 2, "orange": 3}

# iterate over the key-value pairs
for key, value in my_dict.items():
    print(key, value)

# convert the key-value pairs to a list of tuples
my_list = list(my_dict.items())
print(my_list)

# create a new dictionary from the key-value pairs where the values are doubled
new_dict = {key: value * 2 for key, value in my_dict.items()}
print(new_dict)
