# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) < 2:
        return None
    else:
        largest_value = max(values)
        second_largest_value = None
    for value in values:
        if value != largest_value and (second_largest_value is None or value > second_largest_value):
            second_largest_value = value
        return second_largest_value



'''
The second_largest_value should be initalized to 'None' instead of 0
because None represents the absence of a value, while 0 represents a numerical value.

In the find_second_largest function, we are looking for the second largest value in a list of numerical values.
If we initialize second_largest_value to 0, then it is possible that the second largest value in the list is actually 0,
which would cause the function to return an incorrect result.

By initializing second_largest_value to None, we are indicating that we have not yet found a second largest value in the list.
Then, as we iterate through the list of values, we can check if each value is greater than the current second_largest_value.
If it is, we update second_largest_value to be that value.
This way, if there is no second largest value in the list, the function will correctly return None.



This code checks if the length of the list is less than 2, which means there is no second largest value.
Then, it initializes largest_value to be the maximum value in the list, and second_largest_value to be None.

The loop then iterates through each value in the list and checks if it is not equal to largest_value.
If it is not, it checks if second_largest_value is None or if the value is greater than second_largest_value.
If either of these conditions is true, it updates second_largest_value to be the current value.

Finally, after looping through all the values, the function returns second_largest_value.
'''
