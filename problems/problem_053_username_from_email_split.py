# Write a function that meets these requirements.
#
# Name:       username_from_email
# Parameters: a valid email address as a string
# Returns:    the username portion of the email address
#
# The username portion of an email is the substring
# of the email address that appears before the @
#
# Examples
#    * input:   "basia@yahoo.com"
#      returns: "basia"
#    * input:   "basia.farid@yahoo.com"
#      returns: "basia.farid"
#    * input:   "basia_farid+test@yahoo.com"
#      returns: "basia_farid+test"

def username_from_email(email_string):
    return email_string.split("@")[0]   # extract first element


















'''
example returned list = ["basia", "yahoo.com"] -> [0, 1]
The split() method is used to split the email string at the "@" symbol, which returns a list of two strings: the username and the domain name.
The [0] is then used to access the first element of the resulting list, which is the username string.
'''

print(username_from_email("basia@yahoo.com"))
# returns: "basia"
print(username_from_email("basia.farid@yahoo.com"))
# returns: "basia.farid"
print(username_from_email("basia_farid+test@yahoo.com"))
# returns: "basia_farid+test"



# SIDEQUEST

def domain_name_from_email(domain_string):
    return domain_string.split("@")[1]

print(domain_name_from_email("basia@yahoo.com"))
# returns: "yahoo.com"
print(domain_name_from_email("basia.farid@yahoo.com"))
# returns: "yahoo.com"
print(domain_name_from_email("basia_farid+test@yahoo.com"))
# returns: "yahoo.com"
