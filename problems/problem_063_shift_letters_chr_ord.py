# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(word):
    new_word = ""
    for letter in word:
        if letter == "Z":
            new_letter = "A"
        elif letter == "z":
            new_letter = "a"
        else:
            new_letter = chr(ord(letter) + 1)
        new_word += new_letter
    return new_word


print(shift_letters("import"))
# result:  "jnqpsu"
print(shift_letters("ABBA"))
# result: "BCCB"
print(shift_letters("Kala"))
# result:  "Lbmb"
print(shift_letters("zap"))
# result:  "abq"


'''
ord() and chr() are used together to shift each letter in the input word by one position in the ASCII character set.

ord() is used to get the ASCII code of each letter in the input word.
The ord() function takes a single character (a string of length 1) as an argument and returns an integer representing the Unicode code point of that character.
In this case, ord(letter) returns the ASCII code of the current letter being processed in the for loop.

chr() is then used to convert the shifted ASCII code back to a character.
The chr() function takes an integer representing a Unicode code point as an argument and returns the corresponding character.
In this case, chr(ord(letter) + 1) returns the character corresponding to the next ASCII code after the current letter's ASCII code.

The shift_letters() function then concatenates the shifted letters together to form a new string, which is returned as the output of the function.

For example, if we call shift_letters("hello"), the function will shift each letter in the input string by one position in the ASCII character set, resulting in the output string "ifmmp". Here's how the function works step by step:

The first letter in the input string is "h". Its ASCII code is 104.
We add 1 to the ASCII code of "h", resulting in 105.
We use chr(105) to convert the shifted ASCII code back to a character, which is "i".
We concatenate "i" to the new string.
We repeat steps 1-4 for each letter in the input string, resulting in the output string "ifmmp".

Overall, this function provides a simple example of how to shift characters in a string using ord() and chr() in Python.

'''
