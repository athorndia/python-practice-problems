# Write a class that meets these requirements.
#
# Name:       Student
#
# Required state:
#    * name, a string
#
# Behavior:
#    * add_score(score)   # Adds a score to their list of scores
#    * get_average()      # Gets the average of the student's scores
#
# Example:
#    student = Student("Malik")
#
#    print(student.get_average())    # Prints None
#    student.add_score(80)
#    print(student.get_average())    # Prints 80
#    student.add_score(90)
#    student.add_score(82)
#    print(student.get_average())    # Prints 84
#
# There is pseudocode for you to guide you.


# class Student
    # method initializer with required state "name"
        # self.name = name
        # self.scores = [] because its an internal state

    # method add_score(self, score)
        # appends the score value to self.scores

    # method get_average(self)
        # if there are no scores in self.scores
            # return None
        # returns the sum of the scores divided by
        # the number of scores

class Student:
    def __init__(self, name, scores):
        self.name = name
        self.scores = scores

    # Adds a score to their list of scores
    def add_score(self, score):
        self.scores.append(score)

    # Gets the average of the student's scores
    def get_average(self):
        if len(self.scores) == 0:
            return None
        else:
            return sum(self.scores) / len(self.scores)

# Create a new Student object with mock name and empty list
student = Student("Malik", [])

# Print out the name and scores of the student
print(f"Name: {student.name}")
print(f"Scores: {student.scores}")
print(f"Average: {student.get_average()}")    # Prints None

student.add_score(80)
print(f"Average: {student.get_average()}")    # Prints 80
student.add_score(90)
student.add_score(82)
print(f"Average: {student.get_average()}")    # Prints 84

print(student.scores)
