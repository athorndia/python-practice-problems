# Write a function that meets these requirements.
#
# Name: generate_lottery_numbers
# Parameters: none
# Returns:   a list of six random unique numbers between 1 and 40, inclusive
#
# Example bad results:
#  [4, 2, 3, 3, 1, 5] duplicate numbers
#  [1, 2, 3, 4, 5] not six numbers
#
# You can use randint from random, here, or any of
# the other applicable functions from the random
# package.
#
# https://docs.python.org/3/library/random.html

import random

def generate_lottery_numbers():
    numbers = set()
    while len(numbers) < 6:
        numbers.add(random.randint(1, 40))
    return list(numbers)

# Every time the generate_lottery_numbers() function is called, a list of 6 random unique numbers will be printed out as output.
print(generate_lottery_numbers())
print(generate_lottery_numbers())
print(generate_lottery_numbers())
print(generate_lottery_numbers())
print(generate_lottery_numbers())
print(generate_lottery_numbers())

# Can create variable 'lottery_numbers' to store 'generate_lottery_numbers()' function to improve readability. 
lottery_numbers = generate_lottery_numbers()
print(lottery_numbers)

'''
The set() function creates a set object, which is an unordered collection of unique elements.
It can be used with any iterable object including lists, tuples, strings, and dictionaries.

Here we use the set() function to store the lottery numbers.
While the length of numbers in the list are less than 6,
we add a random number between 1 and 40 to the set using the add method, imported random module, and randint(lower_bound, upper_bound_inclusive)...
which only adds the number if it's not already in the set.
'''
