'''
Complete the function make_description below, which takes a person's name and
a dictionary of their attributes and constructs a single string from them.

For example, say the person's name is "Lulu" and their attribute dictionary
contained the following entries:

{
    "hair": "red",
    "eyes": "blue",
}
Then, the return value of the function should be the string:

Lulu, red hair, blue eyes
If the dictionary has no entries, then the return value
should just be the person's name.

'''


def make_description(name, attributes):
    if not attributes:
        return name
    else:
        description = name + ", "
        for key, value in attributes.items():
            description += value + " " + key + ", "
    return description[:-2]


# f-string version:
# def make_description(name, attributes):
#     if not attributes:
#         return name
#     else:
#         description = f"{name}, "
#         for key, value in attributes.items():
#             description += f"{value} {key}, "
#     return description[:-2]


attributes = {
    "hair": "red",
    "eyes": "blue",
}
name = "Lulu"

print(make_description(name, attributes))
# output: Lulu, red hair, blue eyes

'''
In the make_description function, the line return description[:-2] is used
to remove the trailing comma and space from the end of the description string.

When constructing the description string, we add a comma and space after each
attribute value and key, even for the last attribute.
This results in an extra comma and space at the end of the string,
which we don't want.

To remove the extra comma and space, we use slicing to return a substring
of the description string that includes all characters except for the last two.
Specifically, description[:-2] returns a new string that starts at
the beginning of description and ends two characters before the end of
description, effectively removing the last comma and space.

The items() method is a built-in function in Python that returns a view
object that contains the key-value pairs of a dictionary.
Each key-value pair is returned as a tuple, and the view object can be
used in various ways, such as iterating over the key-value pairs
or converting them to a list.

'''


# One way to solve this is like this:

def make_description(name, attributes):
    description = name
    for key, value in attributes.items():
        description = description + ", " + value + " " + key
    return description

# Or, in a more terse way with formatted strings, like this:


def make_description(name, attributes):
    description = name
    for key, value in attributes.items():
        description += f", {value} {key}"
    return description


''' ###############################################################
MAKE IT SO!

Complete the function reverse_entries below that takes a dictionary
and returns a new dictionary whose key-value pairs are the values
and keys from the input.

For example, if the input is this dictionary:

{
    "hair": "red",
    "eyes": "blue",
}
then the output should be this dictionary:

{
    "red": "hair",
    "blue": "eyes",
}
First, try solving this with a for loop.

Then, once you have that figured out, read about list comprehensions
and then try it with a dictionary comprehension.
You have to look at the third example in the "Dictionaries"
section to see an example of a dictionary comprehension.

'''

'''for-loop version

This function creates a new empty dictionary called reversed_dict.
It then iterates through the input dictionary using a for loop, swapping
the keys and values and adding them to the new dictionary.
Finally, it returns the new dictionary.
'''


def reverse_entries(dictionary):
    reversed_dict = {}
    for key, value in dictionary.items():
        reversed_dict[value] = key
    return reversed_dict


dictionary = {
    "hair": "red",
    "eyes": "blue",
}

print(reverse_entries(dictionary))
# output: {'red': 'hair', 'blue': 'eyes'}

'''regular version

Here's how you can implement the same function
using a dictionary comprehension:

This implementation is much more concise than the previous one.
It uses a dictionary comprehension to create a new dictionary directly
from the input dictionary, swapping the keys and values in the process.
The resulting dictionary is then returned.
'''


def reverse_entries(dictionary):
    return {value: key for key, value in dictionary.items()}
