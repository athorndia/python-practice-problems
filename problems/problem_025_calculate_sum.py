# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    if values == []:
        return None
    else:
        total = sum(values)
        return total

'''
Use total instead of sum if you use the sum() function. You cannot use the same name for a function and a variable. 
'''
