# def partition(list, size):
#     chunks = []
#     in_progress = []
#     for item in list:
#         if len(in_progress) == size:
#             chunks.append(in_progress)
#             in_progress = []
#         in_progress.append(item)
#     return chunks

# input = [0, 1, 2, 3, 4, 5, 6]

# result = partition(input, 2)
# print(result)


# def count_matches(items, param2=None):
#     matches = []
#     for item in items:
#         if(item.get("color") == param2):
#             matches.append(item)
#     return len(matches)

# input = [
#     {"background": "green",  "size": 5,  "color": "blue"},
#     {"background": "yellow", "size": 5,  "color": "green"},
#     {"background": "blue",   "size": 25, "color": "green"},
#     {"background": "yellow", "size": 5,  "weight": "light"},
# ]

# result = count_matches(input)
# print(result)


# from datetime import datetime
# class Invoice:
#     def __init__(self, customer_name, amount, invoice_date):
#         self.customer_name = customer_name
#         self.amount_due = amount
#         self.invoice_date = invoice_date

# def amount_due(invoices, days=30):
#     past_due = 0
#     for invoice in invoices:
#         if (datetime.now() - invoice.invoice_date).days > days:
#             past_due += invoice.amount_due
#     return past_due

# invoices = [
#     Invoice("Raul", 25.00, datetime(2010, 4, 15)),
#     Invoice("Poli", 50.00, datetime(2029, 11, 5)),
#     Invoice("Don", 75.00, datetime(2012, 7, 21)),
#     Invoice("Anne", 100.00, datetime(2035, 6, 17))
# ]

# result = amount_due(invoices, 90)
# print(result)
# def find_shipment_weight(shipment):
#     total_weight = sum(item["quantity"] * item["product_weight_pounds"] for item in shipment)
#     return total_weight

#     # total_weight = 0
#     # for item in shipment:
#     #     weight = sum(item["quantity"] * item["product_weight_pounds"])
#     #     total_weight += weight
#     #     return total_weight


# shipment = [
#     {
#         "product_name": "can of soup",
#         "product_weight_pounds": 3.4,
#         "quantity": 3,
#     },
# ]
# print(find_shipment_weight(shipment))

# shipment = [
#     {
#         "product_name": "beans",
#         "product_weight_pounds": 2,
#         "quantity": 5,
#     },
#     {
#         "product_name": "rice",
#         "product_weight_pounds": 1.5,
#         "quantity": 7,
#     },
# ]
# print(find_shipment_weight(shipment))


# def find_longest(strings):
#     """Returns the longest string in a list of strings."""
#     longest_string = None
#     for string in strings:
#         if longest_string is None or len(string) > len(longest_string):
#             longest_string = string
#     return longest_string


# strings = []
# print(find_longest(strings))

# strings = ["a"]
# print(find_longest(strings))

# strings = ["a", "bbb", "cc"]
# print(find_longest(strings))


# ############################################

# def is_multiple_of(number, base):
#     if int(number) != number or int(base) != base:
#         return False
#     if number % base == 0:
#         return True
#     else:
#         return False


#############################################


def shift_cipher(message, shift):
    if not message:
        return ""
    shifted_message = ""
    for letter in message:
        if letter.isalpha():
            if letter.isupper():
                value = ord(letter) - ord('A') + shift
                value %= 26
                shifted_message += chr(value + ord('A'))
            elif letter.islower():
                value = ord(letter) - ord('a') + shift
                value %= 26
                shifted_message += chr(value + ord('a'))
        else:
            shifted_message += letter
    return shifted_message


def shift_cipher(message, shift):
    # Look at each character in message
    # If it's a letter, shift it
    # Transform character using this process:
    # Transform char into a number
    # Add the shift value to the number
    # Transform the number back into a character
    # Add the new char to result
    # Return transformed message

    # Create result var, set to empty string
    result = ""
    # Look at each character in message / loop over message
    for char in message:
        # Transform char into a number using ord
        value = ord(char)
        # Add the shift value to the number
        value += shift
        # Transform the number back into a character using chr
        # add the new char to result
        result += chr(value)
    return result


############################

# def read_delimited(line, separator):
#     if line == "":
#         return [""]
#     else:
#         return line.split(separator)

####################################
