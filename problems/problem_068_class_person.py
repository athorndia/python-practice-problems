# Write a class that meets these requirements.
#
# Name:       Person
#
# Required state:
#    * name, a string
#    * hated foods list, a list of names of food they don't like
#    * loved foods list, a list of names of food they really do like
#
# Behavior:
#    * taste(food name)  * returns None if the food name is not in their
#                                  hated or loved food lists
#                        * returns True if the food name is in their
#                                  loved food list
#                        * returns False if the food name is in their
#                                  hated food list
#
# Example:
#    person = Person("Malik",
#                    ["cottage cheese", "sauerkraut"],
#                    ["pizza", "schnitzel"])
#
#    print(person.taste("lasagna"))     # Prints None, not in either list
#    print(person.taste("sauerkraut"))  # Prints False, in the hated list
#    print(person.taste("pizza"))       # Prints True, in the loved list


# class Person
# method initializer with name, hated foods list, and loved foods list
# self.name = name
# self.hated_foods = hated_foods
# self.loved_foods = loved_foods
# method taste(self, food)
# if food is in self.hated_foods
# return False
# otherwise, if food is in self.loved_foods
# return True
# otherwise
# return None

'''
You can change the instance variables to private variables by adding an underscore before their names.
This is good practice to prevent accidental modification of the variables outside the class

You can remove getter methods since you can access private variables directly using dot notation.

Modify the taste() method to return a string instead of a boolean or None. This will make it more informative and easier to understand.

Change the way you create the Person object by passing the hated_foods and loved_foods lists directly instead of wrapping them in another list.
'''
class Person:
    def __init__(self, name, hated_foods, loved_foods):
        self._name = name
        self._hated_foods = hated_foods
        self._loved_foods = loved_foods

    def taste(self, food):
        if food in self._hated_foods:
            return False, f"{food} is not liked by {self._name}."
        elif food in self._loved_foods:
            return True, f"{food} is liked by {self._name}!"
        else:
            return None, f"{self._name} has no preference for {food}"

# Get name, hated_foods, and loved_foods from the user
name = input("Enter your name: ")
hated_foods = input("Enter 3 of your hated foods (separated by commas): ").split(",")
loved_foods = input("Enter 3 of your loved foods (separated by commas): ").split(",")

# Create a new Person object from user's inputed name, hated_foods, loved_foods
person = Person(name, hated_foods, loved_foods)

# Print out the name
print(person._name)

# Check if a food is liked or not
print(person.taste("pizza"))            # Prints None, not in either list
print(person.taste("spinach"))          # Prints False, in the hated list
print(person.taste("chocolate"))        # Prints True, in the loved list
