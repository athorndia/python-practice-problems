# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    list_for_the_day = []
    if is_workday == True and is_sunny == False:        # alternative line - if is_workday and not is_sunny:
        list_for_the_day.append("umbrella")
    elif is_workday == True:
        list_for_the_day.append("laptop")
    else:
        list_for_the_day.append("surfboard")

    return list_for_the_day
