# Write a function that meets these requirements.
#
# Name:       remove_duplicates
# Parameters: a list of values
# Returns:    a copy of the list removing all
#             duplicate values and keeping the
#             original order
#
# Examples:
#     * input:   [1, 1, 1, 1]
#       returns: [1]
#     * input:   [1, 2, 2, 1]
#       returns: [1, 2]
#     * input:   [1, 3, 3, 20, 3, 2, 2]
#       returns: [1, 3, 20, 2]

def remove_duplicates(list_of_values):
    return list(set(list_of_values))

'''
Remember sets are an unordered collection of unique elements, whereas lists are ordered collections that can contain dublicate elements.
Because sets are unordered, the elements cannot be accessed/extracted using an index like a list.

Here the set() function is used to convert the input 'list list_of_values' into a set, which automatically removes any duplicate elements.
The list() function is used to convert the set back into a list to achieve desired output.

'''

print(remove_duplicates([1, 1, 1, 1]))
# returns: [1]
print(remove_duplicates([1, 2, 2, 1]))
# returns: [1, 2]
print(remove_duplicates([1, 3, 3, 20, 3, 2, 2]))
# Expected: returns: [1, 3, 20, 2]
# Actual: returns: [1, 2, 3, 20]


# Alternative Implementation
def remove_duplicates(list_of_values):
    list_of_unique_values = []
    for value in list_of_values:
        if value not in list_of_unique_values:
            list_of_unique_values.append(value)
    return list_of_unique_values

print(remove_duplicates([1, 1, 1, 1]))
# returns: [1]
print(remove_duplicates([1, 2, 2, 1]))
# returns: [1, 2]
print(remove_duplicates([1, 3, 3, 20, 3, 2, 2]))
# returns: [1, 3, 20, 2]
