# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    if s == "":
        return ""
    else:
        return "".join(set(s))

user_input = input("Enter a string to remove duplicate letters: ")
result = remove_duplicate_letters(user_input)
print(result)

'''
The set() function can be used to create a 'set', which is an unordered collection of unique and immutable elements.
The set() function takes an iterable as its argument and returns a set containing all the unique elements of the iterable.

Sets have a number of built-in methods that you can use to modify or manipulate the set. Some common methods include:

    add(): Adds an element to the set.

    remove(): Removes an element from the set. Raises a KeyError if the element is not in the set.

    discard(): Removes an element from the set if it is present. Does not raise an error if the element is not in the set.

    pop(): Removes and returns an arbitrary element from the set. Raises a KeyError if the set is empty.

    clear(): Removes all elements from the set.

You can also perform various operations with sets, such as union, intersection, and difference, using operators like |, &, and -.


The input() function in Python allows you to get user input from the console. When you call input(),
the program will wait for the user to enter some text and press Enter.
Whatever the user enters will be returned as a string by the input() function.

'''
