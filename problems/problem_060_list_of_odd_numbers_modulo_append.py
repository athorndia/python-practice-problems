# Write a function that meets these requirements.
#
# Name:       only_odds
# Parameters: a list of numbers
# Returns:    a copy of the list that only includes the
#             odd numbers from the original list
#
# Examples:
#     * input:   [1, 2, 3, 4]
#       returns: [1, 3]
#     * input:   [2, 4, 6, 8]
#       returns: []
#     * input:   [1, 3, 5, 7]
#       returns: [1, 3, 5, 7]

def only_odds(list_of_numbers):
    list_of_odd_numbers = []
    for number in list_of_numbers:
        if number % 2 == 1:
            list_of_odd_numbers.append(number)
    return list_of_odd_numbers

# remember to return 'list_of_odd_numbers' at the end of the for-loop (returns multiple elements in list) and not in the if-statement (returns only one element).

print(only_odds([1, 2, 3, 4]))
# returns: [1, 3]
print(only_odds([2, 4, 6, 8]))
# returns: []
print(only_odds([1, 3, 5, 7]))
# returns: [1, 3, 5, 7]
