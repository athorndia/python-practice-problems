# Complete the make_sentences function that accepts three
# lists.
#   * subjects contains a list of subjects for three-word sentences
#   * verbs contains a list of verbs for three-word sentences
#   * objects contains a list of objects for three-word sentences
# The make_sentences function should return all possible sentences
# that can be made from the words in each list
#
# Examples:
#   * subjects: ["I"]
#     verbs:    ["play"]
#     objects:  ["Portal"]
#     returns:  ["I play Portal"]
#   * subjects: ["I", "You"]
#     verbs:    ["play"]
#     objects:  ["Portal", "Sable"]
#     returns:  ["I play Portal", "I play Sable",
#                "You play Portal", "You play Sable"]
#   * subjects: ["I", "You"]
#     verbs:    ["play", "watch"]
#     objects:  ["Portal", "Sable"]
#     returns:  ["I play Portal", "I play Sable",
#                "I watch Portal", "I watch Sable",
#                "You play Portal", "You play Sable"
#                "You watch Portal", "You watch Sable"]
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

def make_sentences(subjects, verbs, objects):
    sentences = []
    for subject in subjects:
        for verb in verbs:
            for obj in objects:
                sentence = f"{subject} {verb} {obj}."
                sentences.append(sentence)
    return sentences


subjects = ["I"]
verbs = ["play"]
objects = ["Portal"]
print(make_sentences(subjects, verbs, objects))
# returns:  ["I play Portal"]


subjects = ["I", "You"]
verbs = ["play"]
objects = ["Portal", "Sable"]
print(make_sentences(subjects, verbs, objects))
# returns:  ["I play Portal", "I play Sable",
#                "You play Portal", "You play Sable"]


subjects = ["I", "You"]
verbs = ["play", "watch"]
objects = ["Portal", "Sable"]
print(make_sentences(subjects, verbs, objects))
#     returns:  ["I play Portal", "I play Sable",
#                "I watch Portal", "I watch Sable",
#                "You play Portal", "You play Sable"
#                "You watch Portal", "You watch Sable"]


'''
This function takes three lists as input: subjects, verbs, and objects.
It then creates a list called sentences to store all the possible sentences that can be made from the words in each list.

The function then uses three nested for loops to iterate over each subject, verb, and object,
and creates a sentence by concatenating them with spaces and a period at the end.
Each sentence is then appended to the sentences list.

Finally, the function returns the sentences list containing all possible sentences that can be made from the words in each list.
'''
