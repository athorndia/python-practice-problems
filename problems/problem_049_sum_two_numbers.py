# Write a function that meets these requirements.
#
# Name:       sum_two_numbers
# Parameters: two numerical parameters
# Returns:    the sum of the two numbers
#
# Examples:
#    * x: 3
#      y: 4
#      result: 7

def sum_two_numbers(num1, num2):
    sum = num1 + num2
    return(sum)

num1 = 3
num2 = 4
print(sum_two_numbers(num1, num2))
# result: 7
# Can use x, y instead of num1, num2
