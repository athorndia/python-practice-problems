# Write a function that meets these requirements.
#
# Name:       specific_random
# Parameters: none
# Returns:    a random number between 10 and 500, inclusive,
#             that is divisible by 5 and 7
#
# Examples:
#     * returns: 35
#     * returns: 105
#     * returns: 70
#
# Guidance:
#   * Generate all the numbers that are divisible by 5
#     and 7 into a list
#   * Use random.choice to select one

import random

def specific_random():
    numbers = []
    for number in range(10, 501):
        if number % 5 == 0 and number % 3 == 0:     # alternative line - if number % 35 == 0        (53 can be used because it's divisible by both 5 and 3)
            numbers.append(number)
    return random.choice(numbers)

print(specific_random())
print(specific_random())
print(specific_random())

'''
The choice() method is a function in the random module of Python that returns a random element from a given iterable (such as a list, tuple, or string).
The syntax for using the choice() method is random.choice(iterable),
where iterable is the sequence of elements from which you want to select a random element.

It's important to note that the choice() method only works with iterables that have a finite number of elements.
If you try to use it with an infinite iterable (such as math.inf), your program will get stuck in an infinite loop.
'''




# Alternative way
def specific_random():
    numbers = []
    for number in range(501):
        if number % 5 == 0 and number % 3 == 0:
            numbers.append(random.randint(10, 500))
        return random.choice(numbers)

print(specific_random())
print(specific_random())
print(specific_random())
