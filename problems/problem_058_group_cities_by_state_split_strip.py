# Write a function that meets these requirements.
#
# Name:       group_cities_by_state
# Parameters: a list of cities in the format "«name», «st»"
#             where «name» is the name of the city, followed
#             by a comma and a space, then the two-letter
#             abbreviation of the state
# Returns:    a dictionary whose keys are the two letter
#             abbreviations of the states in the list and
#             whose values are a list of the cities appearing
#             in that list for that state
#
# In the items in the input, there will only be one comma.
#
# Examples:
#     * input:   ["San Antonio, TX"]
#       returns: {"TX": ["San Antonio"]}
#     * input:   ["Springfield, MA", "Boston, MA"]
#       returns: {"MA": ["Springfield", "Boston"]}
#     * input:   ["Cleveland, OH", "Columbus, OH", "Chicago, IL"]
#       returns: {"OH": ["Cleveland", "Columbus"], "IL": ["Chicago"]}
#
# You may want to look up the ".strip()" method for the string.

def group_cities_by_state(list):
    dictionary = {}
    for item in list:
        state = item.split(",")[1].strip()
        city = item.split(",")[0].strip()
        if state not in dictionary:
            dictionary[state] = []
        dictionary[state].append(city)
    return dictionary

print(group_cities_by_state(["San Antonio, TX"]))
# returns: {"TX": ["San Antonio"]}
print(group_cities_by_state(["Springfield, MA", "Boston, MA"]))
# returns: {"MA": ["Springfield", "Boston"]}
print(group_cities_by_state(["Cleveland, OH", "Columbus, OH", "Chicago, IL"]))
# returns: {"OH": ["Cleveland", "Columbus"], "IL": ["Chicago"]}


'''
Create local variables 'state' and 'city' to store the iterable variable 'item' that's modified by split() method, index extraction, and strip() method.
    split() is a string method that is used to split a string into a list of substrings based on a specified separator.

    [1] is used to extract state
    [0] is used to extract city

    strip() is another string method that is used to remove whitespace characters from the beginning and end of a string.

if state is not in dictionary, add it to dictionary as a key with an empty list as its value.
Then access the dictionary at specified key (stored state) and use append() method to add value (stored city)
Once the for-loop is done iterating, the completed dictionary is returned. 
'''
