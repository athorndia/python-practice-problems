# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4
from fractions import Fraction

def sum_fraction_sequence(number):
    fractions = [Fraction(num, num+1) for num in range(1, number+1)]
    return sum(fractions)

print(sum_fraction_sequence(1))
# returns: 1/2

print(sum_fraction_sequence(2))
# returns: 1/2 + 2/3

print(sum_fraction_sequence(3))
# returns: 1/2 + 2/3 + 3/4

'''
use a list comprehension to create a list of fractions for each number in the range.
We then use the sum() function to add up all the fractions in the list and return the result as a Fraction object.
'''

def sum_fraction_sequence(n):           # solution
    sum = 0                             # solution
    for i in range(1, n + 1):           # solution
        sum += i / (i + 1)              # solution
    return sum                          # solution

print(sum_fraction_sequence(1))
# returns: 1/2

print(sum_fraction_sequence(2))
# returns: 1/2 + 2/3

print(sum_fraction_sequence(3))
# returns: 1/2 + 2/3 + 3/4
