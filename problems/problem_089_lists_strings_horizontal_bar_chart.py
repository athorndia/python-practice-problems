'''
Complete the function horizontal_bar_chart below that takes in a string and
creates a horizontal bar chart made out of the instances of the letters in
that string based on the number of each letter.

The return value is a list of the strings of the letters in alphabetical order.

For example, given the sentence "abba has a banana", it would return
this list of strings because there are seven "a", three "b", one "h", two "n",
and one "s" letters in it:

[
  "aaaaaaa",
  "bbb",
  "h",
  "nn",
  "s",
]
All letters in the input will be lowercase letters.

You may want to look at the methods for dictionaries and the
built-in functions. Remember, you can also compare strings with >= and <=.

'''


def horizontal_bar_chart(sentence):
    # Initialize a dictionary to keep track of letter counts
    letter_counts = {}

    # Iterate over each character in the sentence
    for char in sentence:
        # If the character is a letter, add it to the dictionary or
        # increment its count
        if char.isalpha():
            if char in letter_counts:
                letter_counts[char] += 1
            else:
                letter_counts[char] = 1

    # Sort the keys of the dictionary in alphabetical order
    sorted_keys = sorted(letter_counts.keys())

    # Initialize a list to hold the bar strings
    bars = []

    # Iterate over the sorted keys and create a bar string for each letter
    for key in sorted_keys:
        count = letter_counts[key]
        bar = key * count
        bars.append(bar)

    # Return the list of bar strings
    return bars


sentence = "abba has a banana"
bars = horizontal_bar_chart(sentence)
print(bars)

# output: ['aaaaaaa', 'bbb', 'h', 'nn', 's']


# Alternative method:

def horizontal_bar_chart(sentence):
    d = {}
    for c in sentence:
        if c >= "a" and c <= "z":
            if c not in d:
                d[c] = ""
            d[c] += c
    result = []
    for s in d.values():
        result.append(s)
    return list(sorted(result))
