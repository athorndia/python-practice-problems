# Write a class that meets these requirements.
#
# Name:       Person
#
# Required state:
#    * name, a string
#    * hated foods list, a list of names of food they don't like
#    * loved foods list, a list of names of food they really do like
#
# Behavior:
#    * taste(food name)  * returns None if the food name is not in their
#                                  hated or loved food lists
#                        * returns True if the food name is in their
#                                  loved food list
#                        * returns False if the food name is in their
#                                  hated food list
#
# Example:
#    person = Person("Malik",
#                    ["cottage cheese", "sauerkraut"],
#                    ["pizza", "schnitzel"])
#
#    print(person.taste("lasagna"))     # Prints None, not in either list
#    print(person.taste("sauerkraut"))  # Prints False, in the hated list
#    print(person.taste("pizza"))       # Prints True, in the loved list
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

class Person:
    def __init__(self, name, hated_foods, loved_foods):
        self._name = name
        self._hated_foods = hated_foods
        self._loved_foods = loved_foods

    def taste(self, food):
        if food in self._hated_foods:
            return False
        elif food in self._loved_foods:
            return True
        else:
            return None

# Create a new Person object with the mock data
person = Person("Malik", ["cottage cheese", "sauerkraut"], ["pizza", "schnitzel"])

# Check if person has a taste for mock food items
print(person.taste("lasagna"))     # Prints None, not in either list
print(person.taste("sauerkraut"))   # Prints False, in the hated list
print(person.taste("pizza"))       # Prints True, in the loved list

''' BETTER version is in problem_068_class_person.py '''
