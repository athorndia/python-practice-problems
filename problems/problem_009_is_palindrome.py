# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    reversed_word = ''.join(reversed(word))
    if word == reversed_word:
        return True
    else:
        return False

'''
The reversed() function returns a reverse iterator and not a reversed string. Therefore, the comparison between word and reversed(word) will always be False.
You can convert the reversed iterator to a string using the join() method and then compare it with the original string.

Alternatively, you can simply check if the string is equal to its reverse using slicing. Here's another implementation:
'''

def is_palindrome_two(word):
    return word == word[::-1]

'''
This implementation uses slicing to reverse the string and compare it with the original string.
'''
