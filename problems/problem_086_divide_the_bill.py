'''
Your friends and you have gone out to dinner and all ordered the same foods. Now, it's time to split the bill.

Complete the function below to take the total of the bill, figure out the tip, and divide that combined total by the number of diners. Return the final calculation.

For example, if the bill is $20 and the tip is 20% (0.2), then the bill plus the tip is $20 + $4 for $24. If there were two of you, the return value of the function is 12 (24/2).

'''

def divide_bill(bill, num_diners, tip=0.2):
    #Split a bill evenly among diners and add a tip percentage (default 20%)
    total_with_tip = bill * (1 + tip)
    per_person = total_with_tip / num_diners
    return per_person

# def divide_bill(bill, num_diners, tip=0.2):
#     return bill * (1 + tip) / num_diners
